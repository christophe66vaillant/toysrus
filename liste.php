<?php 
    require_once 'app/config.php'; 
    require_once 'app/app-start.php'; 
?>

<?php require_once 'app/controllers/list-controller.php'; ?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tous les jouets - Toys"R"Us</title>

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <link rel="stylesheet" href="/app/style/style.css">

    <link rel="shortcut icon" href="/app/img/favicon.ico" type="image/x-icon">
</head>
<body>
    <div id="pricipal-div">
        <a href="index.php">
            <img id="logo" src="/app/img/logo.png">
        </a>

        <?php require_once 'app/inc/menu.php' ?>

        <h1 class="title">Les jouets</h1>
        <div class="align">
            <?php echo displayFormPrice(); ?>
        </div>

        <div id="article-container">
            <?php echo displaySelectPrice(); ?>
        </div>
    </div>
</body>
</html>