<?php

function displayBrand()
{
    $html = '';
    global $mysql;

    $req = sprintf( 
        'SELECT brands.*, SUM(toys.brand_id = brands.id) AS total
            FROM brands
            JOIN toys ON toys.brand_id = brands.id
            GROUP BY brands.id
            ORDER BY brands.id' );

    if( $result = mysqli_query( $mysql, $req ) ){
        while( $row = mysqli_fetch_assoc( $result ) ){
            $html .= sprintf( '<a href="filtre_liste.php?brand_id=%s">%s (%s)</a>', $row[ 'id' ], $row[ 'name' ], $row[ 'total' ] );
        }
        return $html;
    }
}
