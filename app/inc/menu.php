<?php require_once 'menu-code.php'; ?>

<nav id="menu-holder">
    <ul>
        <li class="menu-all-games">
            <a href="liste.php">Tous les jouets</a>
        </li>
    </ul>
    <ul>
        <li class="menu-brand-choice">
            <a href="#">
                Par marque <i class="fas fa-sort-down"></i>
            </a>
            <ul>
                <?php echo displayBrand(); ?>
            </ul>
        </li>
    </ul>
</nav>

<script src="/app/js/script.js"></script>