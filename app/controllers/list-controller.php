<?php

function displaySelectPrice(){

    $html = '';
    global $mysql;

    if( $_GET[ 'price' ] === '1' ){
        $req = sprintf( 'SELECT id, `image`, `name`, price FROM toys ORDER BY price DESC' );
        if( $result = mysqli_query( $mysql, $req ) ){
            while( $row = mysqli_fetch_assoc( $result ) ){
                $html .= sprintf( '
                <div class="article">
                    <a href="detail.php?id=%s">
                        <img cass="article-name" src="app/img/%s" alt="">
                        <p class="article-name">%s</p>
                    </a>
                    <span class="article-price">%s</span>
                </div>', $row[ 'id' ], $row[ 'image' ], $row[ 'name' ], $row[ 'price' ] );
            }
            return $html;
        }
    }elseif( $_GET[ 'price' ] === '2' ){
        $req = sprintf( 'SELECT id, `image`, `name`, price FROM toys ORDER BY price ASC' );
        if( $result = mysqli_query( $mysql, $req ) ){
            while( $row = mysqli_fetch_assoc( $result ) ){
                $html .= sprintf( '
                <div class="article">
                    <a href="detail.php?id=%s">
                        <img cass="article-name" src="app/img/%s" alt="">
                        <p class="article-name">%s</p>
                    </a>
                    <span class="article-price">%s</span>
                </div>', $row[ 'id' ], $row[ 'image' ], $row[ 'name' ], $row[ 'price' ] );
            }
            return $html;
    }
}else{
        $req = sprintf( 'SELECT id, `image`, `name`, price FROM toys' );
        if( $result = mysqli_query( $mysql, $req ) ){
            while( $row = mysqli_fetch_assoc( $result ) ){
                $html .= sprintf( '
                <div class="article">
                    <a href="detail.php?id=%s">
                        <img cass="article-name" src="app/img/%s" alt="">
                        <p class="article-name">%s</p>
                    </a>
                    <span class="article-price">%s</span>
                </div>', $row[ 'id' ], $row[ 'image' ], $row[ 'name' ], $row[ 'price' ] );
            }
            return $html;
        }
    }
}

function displayFormPrice(){
    error_reporting( E_ALL );
    ini_set( 'display_errors', '0' );

    $html = '';
    global $mysql;

    if( $_GET[ 'price' ] === '1' ){
        $html .= sprintf( 
        '<form>
            <select name="price">
                <option value="1" selected="selected">Plus cher au moins cher</option>
                <option value="2">Moins cher au plus cher</option>
            </select>
            <button type="submit">Valider</button>
        </form>' );
    }elseif( $_GET[ 'price' ] === '2' ){
        $html .= sprintf( 
            '<form>
                <select name="price">
                    <option value="1">Plus cher au moins cher</option>
                    <option value="2" selected="selected">Moins cher au plus cher</option>
                </select>
                <button type="submit">Valider</button>
            </form>' );
    }else{
        $html .= sprintf( 
            '<form>
                <select name="price">
                    <option value="1">Plus cher au moins cher</option>
                    <option value="2">Moins cher au plus cher</option>
                </select>
                <button type="submit">Valider</button>
            </form>' );
    }
    return $html;
}
