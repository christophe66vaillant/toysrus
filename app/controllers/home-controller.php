<?php

function displayTop3()
{
    $html = '';
    global $mysql;

    $req = sprintf( 'SELECT toys.`id`, toys.`image`, toys.`name`, toys.`price`, SUM(sales.quantity) AS total
                FROM toys
                JOIN sales ON toys.id = sales.toy_id
                GROUP BY toys.id
                ORDER BY total DESC
                LIMIT 3' );

    if( $result = mysqli_query( $mysql, $req ) ){
        while( $row = mysqli_fetch_assoc( $result ) ){
        
            $html .= sprintf( ' 
            <div class="article">
                <a href="detail.php?id=%s">
                    <img class="article-img" src="app/img/%s" alt="">
                    <p class="article-name">%s</p>
                </a>
                <span class="article-price">%s €</span>
            </div>', $row['id'], $row['image'], $row[ 'name' ], $row['price']);
        }
        return $html;
    }
}
