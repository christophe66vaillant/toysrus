<?php

function displayFiltredList()
{
    $id = $_GET[ 'brand_id' ];

    $html = '';
    global $mysql;

    $req = sprintf( "SELECT * FROM toys WHERE brand_id LIKE '$id'" );

    if( $result = mysqli_query( $mysql, $req ) ){
        while( $row = mysqli_fetch_assoc( $result ) ){
            $html .= sprintf( '
            <div class="article">
                <a href="detail.php?id=%s">
                    <img cass="article-name" src="app/img/%s" alt="">
                    <p class="article-name">%s</p>
                    </a>
                    <span class="article-price">%s €</span>
            </div>', $row[ 'id' ], $row[ 'image' ], $row[ 'name' ], $row[ 'price' ] );
        }
        return $html;
    }
}

function displayCategoryName()
{

    $id = $_GET[ 'brand_id' ];

    $html = '';
    global $mysql;

    $req = sprintf( "SELECT brands.name FROM toys JOIN brands ON brands.id = '$id' LIMIT 1" );

    if( $result = mysqli_query( $mysql, $req ) ){
        while( $row = mysqli_fetch_assoc( $result ) ){
            $html .= sprintf( '%s', $row[ 'name' ] );
        }
        return $html;
    }

}

function displayBrands(){
    error_reporting(E_ALL);
    ini_set('display_errors', '0');

    $id = $_GET[ 'id' ];

    $brandid = $_GET[ 'brand_id' ];

    global $mysql;
    $html = '';

    $req = sprintf( 'SELECT * FROM brands' );

    if( $result = mysqli_query( $mysql, $req ) ){
        while( $row = mysqli_fetch_assoc( $result ) ){
            if(!empty( $brandid === $row[ 'id' ] ) ){
                $html .= sprintf( '<option selected="selected" value="%s">%s</option>', $row[ 'id' ], $row[ 'name' ] );
                continue;
            }else
                $html .= sprintf( '<option value="%s">%s</option>', $row[ 'id' ], $row[ 'name' ] );
        }
        return $html;
    }
}

function displaySelect(){
    $id = $_GET[ 'brand_id' ];
    global $mysql;

    $html = '';


    $html .= sprintf('<div class="align"><form><select name="brand_id"><option value="1">Choisissez une marque : </option>' . displayBrands() . '</select><button type="submit">Valider</button></form></div>');

    return $html;

}
