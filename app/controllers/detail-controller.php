<?php


function displayDetails()
{
    $html = '';
    global $mysql;

    $id = $_GET[ 'id' ];

    $req = sprintf( 'SELECT  `name`, `image`, price, `description` FROM toys WHERE `id` = ? ');

    if( $stmt = mysqli_prepare( $mysql, $req ) ){

        mysqli_stmt_bind_param( $stmt, 's', $id );
        mysqli_stmt_execute( $stmt );

        $result = mysqli_stmt_get_result( $stmt );

        while( $row = mysqli_fetch_assoc( $result ) ){
            $html .= sprintf( '<div class="details-name title">%s</div>', $row[ 'name' ] );

            $html .= sprintf( '<div class="div-prod">
            <div class="details-img">
                <img src="app/img/%s" alt="">', $row[ 'image' ]);

            $html .= sprintf( '<div class="left-div">
            <p class="details-price">%s €</p>
            <form><select name="store"><option value="">Choisissez un magasin</option>' . displayStores() . '</select><input type="hidden" value=' . $id . ' name="id"><button type="submit">Valider</button></form>
            <span class="details-stock"><span class="details-color">Stock : </span>' . displayStock() . '</span></div></div>
            ', $row[ 'price' ] );

            $html .= sprintf( '<div class="right-div">
                <span class="details-brand"><span class="brand-color">Marque :</span> ' . displayBrandDetails() . '</span>');

            $html .= sprintf( '<div class="details-desc"><br>
                %s</div>
                </div>
            </div>', $row[ 'description' ] );
        }
        return $html;
    }

}

function displayBrandDetails(){

    $html = '';
    global $mysql;

    $id = $_GET[ 'id' ];

    $req = sprintf( 'SELECT brands.name FROM brands JOIN toys ON brand_id = brands.id WHERE toys.id = ?' );

    if( $stmt = mysqli_prepare( $mysql, $req ) ){
        mysqli_stmt_bind_param( $stmt, 's', $id );
        mysqli_stmt_execute( $stmt );
        
        $result = mysqli_stmt_get_result( $stmt );

        while( $row = mysqli_fetch_assoc( $result ) ){
            $html .= sprintf( '%s', $row[ 'name' ] );
        }
        return $html;
    }
}

function displayStock(){
    
    $id = $_GET[ 'id' ];
    $html = '';
    global $mysql;
        if( !empty($_GET[ 'store' ]) ){
            $req = sprintf( 
                'SELECT toys.name,  SUM(stock.quantity) AS total
                FROM toys
                JOIN stock ON toys.id = stock.toy_id WHERE toys.id = ? AND stock.store_id = ?
                GROUP BY toys.id
                ORDER BY total DESC' );
    
            if( $stmt = mysqli_prepare( $mysql, $req ) ){
                $store_id = $_GET[ 'store' ];
    
                mysqli_stmt_bind_param( $stmt, 'ss', $id, $store_id );
                mysqli_stmt_execute( $stmt );
    
                $result = mysqli_stmt_get_result( $stmt );
    
                while( $row = mysqli_fetch_assoc( $result ) ){
                    $html .= sprintf( '%s', $row[ 'total' ] );
                }
                return $html;
            }
    }else{
        $req = sprintf( 
            'SELECT toys.name, SUM(stock.quantity) AS total
                FROM toys
                JOIN stock ON toys.id = stock.toy_id WHERE toys.id = ?
                GROUP BY toys.id
                ORDER BY total DESC' );

        if( $stmt = mysqli_prepare( $mysql, $req ) ){

            mysqli_stmt_bind_param( $stmt, 's', $id );
            mysqli_stmt_execute( $stmt );

            $result = mysqli_stmt_get_result( $stmt );

            while( $row = mysqli_fetch_assoc( $result ) ){
                $html .= sprintf( '%s', $row[ 'total' ] );
            }
            return $html;
        }
    }
}

function displayStores(){
    error_reporting(E_ALL);
    ini_set('display_errors', '0');

    $store = $_GET[ 'store' ];

    global $mysql;
    $html = '';

    $req = sprintf( 'SELECT * FROM stores' );

    if( $result = mysqli_query( $mysql, $req ) ){
        while( $row = mysqli_fetch_assoc( $result ) ){
            if(!empty( $store === $row[ 'id' ] ) ){
                $html .= sprintf( '<option selected="selected" value="%s">%s</option>', $row[ 'id' ], $row[ 'name' ] );
                continue;
            }else{
                $html .= sprintf( '<option value="%s">%s</option>', $row[ 'id' ], $row[ 'name' ] );
            }
        }
        return $html;
    }
}

//     <div class="details-desc">%s</div>
// </div>

// <div class="details-price-stock"><p class="details-price">%s €</p>
//                 <p class="details-stock">Stock : </p></div></div>
//                 <div class="details-brand-desc"><span class="detail-brand">Marque : </span>
//                 <p class="details-desc">%s</p></div>', $row[ 'name' ], $row[ 'image' ], $row[ 'price' ], $row[ 'description' ] ); 