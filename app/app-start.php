<?php

// Constantes de chemins
define( 'PATH_CONTROLLERS', 'app/controllers/' );
define( 'PATH_VIEWS', 'app/views/' );
define( 'PATH_MODELS', 'app/models/' );

//  Démarrage de la session
session_start();

$mysql = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

if( $mysql->connect_error ){
    die( 'Connexion impossible : ' . $mysql->connect_error );
}
