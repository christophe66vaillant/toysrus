<?php 
    require_once 'app/config.php'; 
    require_once 'app/app-start.php'; 
?>

<?php 
    require_once 'app/controllers/filtre_liste-controller.php'; 
?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> <?php echo displayCategoryName(); ?> - Toys"R"Us</title>
    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <link rel="stylesheet" href="/app/style/style.css">

    <link rel="shortcut icon" href="/app/img/favicon.ico" type="image/x-icon">
</head>
<body>
    <div id="pricipal-div">
        <a href="index.php">
            <img id="logo" src="/app/img/logo.png">
        </a>

        <?php require_once 'app/inc/menu.php' ?>

        <h1 class="title">Voici les jouets de la marque "<?php echo displayCategoryName(); ?>" ! </h1>
        <?php echo displaySelect(); ?>

        <div id="article-container">
            <?php echo displayFiltredList(); ?>
        </div>
    </div>
</body>
</html>