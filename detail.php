<?php 
    require_once 'app/config.php'; 
    require_once 'app/app-start.php'; 
?>

<?php require_once 'app/controllers/detail-controller.php'; ?>

<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Détails - Toys"R"Us</title>

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <link rel="stylesheet" href="/app/style/style.css">

    <link rel="shortcut icon" href="/app/img/favicon.ico" type="image/x-icon">
</head>
<body>
    <div id="pricipal-div">
        <a href="index.php">
            <img id="logo" src="/app/img/logo.png">
        </a>

        <?php include_once 'app/inc/menu.php' ?>

    <div class="details-article">
        <?php echo displayDetails(); ?>
    </div>
        
</div>
</body>
</html>